export default class CSet {
  // construct a new "CSet" and specify a bias, defaulting to 'add'
    constructor({
      bias: bias = 'add',
    } = {}) {
      this.added = {
        elements: new Set(),
        timestamps: new Map()
      };
      this.removed = {
        elements: new Set(),
        timestamps: new Map()
      };
      this.bias = bias;
    }
  // membership functions
    has(element) {
      const removed = this.removed.elements.has(element);
      const added = this.added.elements.has(element);
      if ( removed ) {
        if ( added ) {
          const addedTS = this.added.timestamps.get(element);
          const removedTS = this.removed.timestamps.get(element);
          if ( addedTS > removedTS ) return true;
          else if ( addedTS < removedTS ) return false;
          else return this.bias === 'add';
        } else {
          return false;
        }
      } else return added;
    }
    members() {
      return [...this.added.elements].filter(e => this.has(e));
    }
  // property getters 
    get size() {
      return this.members().length;
    }
    get [Symbol.iterator]() {
      return function*() {
        const members = this.members();
        for( const member of members ) yield member;
      }
    }
  // addition and removal 
    add(element, timestamp) {
      if ( timestamp === undefined ) throw new TypeError(`Must provide a timestamp as second argument.`);
      this.added.elements.add(element);
      this.added.timestamps.set(element,timestamp);
      return this;
    }
    delete(element, timestamp) {
      if ( timestamp === undefined ) throw new TypeError(`Must provide a timestamp as second argument.`);
      this.removed.elements.add(element);
      this.removed.timestamps.set(element,timestamp);
      return this;
    }
  // merge with another CSet and return a NEW CSet (with bias set to the argument's bias)
    union(cset) {
      const result = new CSet({bias:cset.bias});
      this.added.elements.forEach(e => result.add(e,this.added.timestamps.get(e)));   
      cset.added.elements.forEach(e => result.add(e,cset.added.timestamps.get(e)));   
      this.removed.elements.forEach(e => result.delete(e,this.removed.timestamps.get(e))); 
      cset.removed.elements.forEach(e => result.delete(e,cset.removed.timestamps.get(e)));  
      return result;
    }
}
