import CSet from './cset.js';

const A = 'A';
const B = 'B';

const t0 = 0;
const t1 = 1;

const tests = [
  {
    desc: `Basic add test. Test if adding an element works.`,
    test: () => (new CSet).add(A, t0).members(),
    mark: r => r[0] === A
  },
  {
    desc: `Basic delete test. Test if removing an element works.`,
    test: () => (new CSet).delete(A, t0).members(),
    mark: r => r[0] === undefined
  },
  {
    desc: `Bias test. Default bias is 'add'`,
    test: () => (new CSet).add(A,t0).delete(A,t0).members(),
    mark: r => r[0] === A 
  },
  {
    desc: `Bias test. Set bias to delete and test.`,
    test: () => (new CSet({bias:'delete'})).add(A,t0).delete(A,t0).members(),
    mark: r => r[0] === undefined
  },
  {
    desc: `Last delete wins? Check if a delete, at time after an add, but recorded before the add, correctly deletes.`,
    test: () => (new CSet).delete(A,t1).add(A,t0),
    mark: r => r[0] === undefined
  },
  {
    desc: `Last add wins? Check if an add, at time after an delete, but recorded before the delete, correctly adds.`,
    test: () => (new CSet).add(A,t1).delete(A,t0).members(),
    mark: r => r[0] === A
  },
  {
    desc: `Test size`,
    test: () => (new CSet).add(A,t0).add(B,t1).delete(A,t1).size,
    mark: r => r == 1
  },
  {
    desc: `Test iterator.`,
    test: () => (new CSet).add(A,t0).add(B,t0),
    mark: r => {
      const x = [...r];
      return x[0] == A && x[1] == B;
    }
  },
  {
    desc: `Test union with an empty CSet.`,
    test: () => (new CSet).add(A,t0).add(B,t0).delete(A,t1).union(new CSet).members(),
    mark: r => r.length == 1 && r[0] == B
  },
  {
    desc: `Test union with a non-empty CSet.`,
    test: () => (new CSet).add(A,t0).add(B,t0).union((new CSet).delete(A,t1)).members(),
    mark: r => r.length == 1 && r[0] == B
  }
]

run_tests();

function run_tests() {
  const results = tests.map(({test,mark,desc}) => ({pass: mark(test()), desc}));
  const scorecard = {
    allPass: results.every(({pass}) => pass),
    totalTests: results.length,
    passedTests: results.reduce((score,{pass}) => score + (pass ? 1 : 0), 0)
  };
  scorecard.percentageScore = `${(100.0*scorecard.passedTests/scorecard.totalTests).toFixed(2)}%`
  scorecard.tallyScore = `${scorecard.passedTests}/${scorecard.totalTests}`
  console.log(JSON.stringify({results,scorecard},null,2));
}
