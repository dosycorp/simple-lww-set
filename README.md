# simple-lww-set (1.0.3)

Simple implementation of last-write-wins element set

## Testing

Get the code either cloning:

```shell
$ git clone https://github.com/crislin2046/simple-lww-set.git
```

Or from npm:

```shell
$ npm i simple-lww-set
```

Then run the tests for node:

```shell
$ cd simple-lww-set
$ npm i && npm test
```

Alternately, to test in a browser:

```shell
$ npm run browser-test
```


